import { connect } from 'react-redux';
import BookedTicket from '../Components/BookedTicket_Comp';


const mapStateToProps = state => {
  const { tickets } = state;
  const { data, currentNumber } = tickets;
  return{
    data,
    currentNumber,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch
  }
}

const ConnectedBookedTicket = connect(mapStateToProps, mapDispatchToProps)(BookedTicket);

export default ConnectedBookedTicket;
