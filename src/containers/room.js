import { connect } from 'react-redux';
import Room from '../Components/Room';


const mapStateToProps = state => {
  const { tickets } = state;
  const { data, currentNumber } = tickets;
  return{
    data,
    currentNumber,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch
  }
}

const ConnectedRoom = connect(mapStateToProps, mapDispatchToProps)(Room);

export default ConnectedRoom;
