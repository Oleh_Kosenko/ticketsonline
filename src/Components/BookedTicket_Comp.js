import React from 'react';

class BookedTicket extends React.Component {
    removeTicket = e => {
        let { dispatch, booked, data, currentNumber } = this.props;
        let { id } = e.target.dataset;
        id = Number(id);

        const { all } = data[currentNumber];
        booked = booked.filter(place => place.id !== id);

        dispatch({
            type: 'TICKET',
            data: {
                booked,
                all,
            }
        })
    }
    render() {
        let { id, row, seat, price } = this.props;

        return (
            <li style={{color: "darkblue"}}>
                ряд {+row + 1} меcто {+seat + 1} {price} грн.
                <label
                    data-id={id}
                    onClick={this.removeTicket}
                    className="del">✗
                </label>
            </li>
        )
    }
}

export default BookedTicket;
