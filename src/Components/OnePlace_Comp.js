import React from 'react';
import './../styles.css';

class OnePlace extends React.Component {
    getStyle = (rowIndex, seatIndex, rowsLength, free) => {
        let className = "";
        if (!free) {
            className = "placeImgTaken "
        }
        if (rowIndex === (rowsLength - 1)) {
            if (seatIndex % 2 === 0)
                return className + "placeImg tooltip plImg40";
            else
                return className + "placeImg tooltip plImg2";
        } else if (seatIndex % 4 === 0)
            return className + "placeImg tooltip plImg20";
        else
            return className + "placeImg tooltip plImg4";
    }

    selectPlace = e => {
        let { id, row, seat, price, free } = e.target.dataset;
        id = Number(id);
        row = Number(row);
        seat = Number(seat);
        price = Number(price);
        if (free == "true" && !isNaN(price)) {
            e.target.dataset.free = "false";
            let { dispatch } = this.props;

            dispatch({
                type: 'BOOK',
                id,
                row,
                seat,
                price,
            })
        }
    }

    render() {
        let {id, rowIndex, seatIndex, rowsLength, price, booked} = this.props;
        let free = (Array.isArray(booked)) && !booked.find(place => place.id == id);

        return (
                <div
                    id={id}
                    onClick={this.selectPlace}
                    className={this.getStyle(rowIndex, seatIndex, rowsLength, free)}
                    data-id={id}
                    data-row={rowIndex}
                    data-seat={seatIndex}
                    data-price={price}
                    data-free={free}
                >
                    <span className="tooltiptext">
                        ряд {rowIndex + 1} место {seatIndex + 1}
                    </span>
                </div>
        )
    }
}

export default OnePlace;
