import React from 'react';
import config from './../config.json';
import generatePlaces from './../generatePlaces';
import OneRow from './OneRow';
import BookedTickets from './../containers/bookedtickets';
import Basket from './Basket';
import BasketString from './BasketString';
import './../styles.css';

class Room extends React.Component {
    constructor(props) {
        super(props);
        let {dispatch} = props;
        let dataFromStorage = localStorage.getItem("tickets");
        if (Boolean(dataFromStorage)) {
            dataFromStorage = JSON.parse(dataFromStorage)
            dispatch({
                type: "ALL_DATA",
                allData: dataFromStorage
            })
        }
    }

    state = {places: []};

    updateNumber = () => {
        let {dispatch} = this.props;
        const number = Number(this.props.match.params.number);

        dispatch({
            type: 'SET_CURRENT_NUMBER',
            data: {
                number: number
            }
        })
    }

    price = priceType => {
        const conf = config[Number(this.props.match.params.number)];
        let { cheapPrice, expensivePrice, vipPrice } = conf;
        switch (priceType) {
            case "cheap": return cheapPrice;
            case "expensive": return expensivePrice;
            case "vip": return vipPrice;
            default: return cheapPrice;
        }
    }

    render() {
        const conf = config[Number(this.props.match.params.number)];

        this.updateNumber();

        const currentNumber = Number(this.props.match.params.number);
        let places;
        let { dispatch } = this.props;
        let { data } = this.props;
        if (data[currentNumber].all.length === 0) {
            const newPlaces = generatePlaces(conf.rows, conf.places);
            dispatch({
                type: "TICKET",
                data: {all: newPlaces}
            });
            places = newPlaces;
        } else {
            places = data[currentNumber].all;
        }

        document.body.style.backgroundColor = conf.background;

        let { name } = conf;
        let rowsLength = places.rows.length;

        return (
            <div>
                <h3 style={{color: conf.titleColor}}>{name}</h3>
                <div className="wrap" id="app">
                    <div className="places__app">
                        <Basket/>
                        <div>
                            <BasketString/>
                            <BookedTickets/>
                        </div>
                        <div align="center">
                            <h5 style={{color: "darkblue"}}>
                                Экран
                            </h5>
                            {
                                places.rows.map((row, rowIndex) => {
                                    return <OneRow
                                        key={rowIndex}
                                        rowIndex={rowIndex}
                                        seats={row.seats}
                                        rowsLength={rowsLength}
                                        price={this.price(row.priceType)}/>
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Room;
