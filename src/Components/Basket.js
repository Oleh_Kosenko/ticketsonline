import React from 'react';
import store from './../store';

class Basket extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bookedCount: 0
        }
        store.subscribe(this.updateState);
    }

    updateState = () => {
        let { data, currentNumber } = store.getState().tickets;
        this.setState({
            bookedCount: data[currentNumber].booked.length
        })
    }

    render() {
        return (
            <div className="places__icon" id="places__icon">
                <img alt="" src="https://image.flaticon.com/icons/svg/23/23258.svg"/>
                <span id="places__cart">{this.state.bookedCount}</span>
            </div>
        )
    }
}

export default Basket;
