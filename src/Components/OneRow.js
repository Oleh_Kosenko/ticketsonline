import React from 'react';
import OnePlace from './../containers/oneplace';

class OneRow extends React.Component {
    render() {
        let {seats, rowIndex, rowsLength, number, price} = this.props;

        return (
            <div>
                {
                    seats.map((seat, seatIndex) => (
                        <OnePlace
                            key={seatIndex}
                            id={seat.id}
                            seatIndex={seatIndex}
                            rowIndex={rowIndex}
                            free={seat.free}
                            price={price}
                            rowsLength={rowsLength}
                            number={number}
                        />
                        )
                    )
                }
            </div>
        )
    }
}

export default OneRow;
