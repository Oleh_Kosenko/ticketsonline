import React from 'react';
import store from './../store';

class BasketString extends React.Component {
    constructor(props) {
        super(props);
        store.subscribe(this.updateState);
    }

    state = {
        bookedCount: 0,
        sum: 0
    }

    updateState = () => {
        let sum = 0;
        store.getState().tickets.data[store.getState().tickets.currentNumber].booked.forEach(ticket=>{
            sum += Number(ticket.price);
        })

        this.setState({
            bookedCount: store.getState().tickets.data[store.getState().tickets.currentNumber].booked.length,
            sum: sum
        })
    }

    render() {
        let {bookedCount, sum} = this.state;
        return (
            <h2 style={{color: "darkblue", width: "300px"}}>У вас в корзине <p>{(bookedCount > 0)?`${bookedCount} билет на сумму ${sum} грн`:"пусто"}</p></h2>
        )
    }
}

export default BasketString;