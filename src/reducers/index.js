import { combineReducers } from 'redux';
import config from './../config.json';

const init = {currentNumber: 0, data: [], currentPlace: {}};

config.forEach(item => {
    init.data.push({
        booked: [],
        all: []
    })
})

const tickets = (state = init, action) => {
    switch (action.type) {
        case 'ALL_DATA': {
            return Object.assign({}, state, action.allData);
        }
        case 'TICKET': {
            const { data } = action;
            const { currentNumber } = state;
            const newState = Object.assign({}, state);
            if (data.booked) newState.data[currentNumber].booked = data.booked;
            if (data.all) newState.data[currentNumber].all = data.all;
            return newState;
        }
        case 'BOOK': {
            let { data, currentNumber } = state;
            const { id, row, seat, price } = action;
            const booked = [...data[currentNumber].booked, { id, row, seat, price }]
            const newState = Object.assign({}, state);
            newState.data = data;
            newState.data[currentNumber].booked = booked;
            return newState;
        }
        case 'SET_CURRENT_NUMBER': {
            const newState = Object.assign({}, state);
            newState.currentNumber = action.data.number;
            return newState;
        }
        default:
            return state;
    }
}


const reducer = combineReducers({
    tickets
});

export default reducer;
