import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import store from './store';

import {BrowserRouter, Route, Switch} from 'react-router-dom';

import OneLink from './Components/OneLink';
import Room from './containers/room';

import Config from './config.json';

const supportsHistory = 'pushState' in window.history;

class App extends React.Component {
    render() {
        return (
                <Provider store={store}>
                    <BrowserRouter forceRefrech={!supportsHistory}>
                        <div>
                            <Switch>
                                <Route path="/room/:number" component={Room}/>
                            </Switch>
                            {
                                Config.map((item, index) => {
                                    return <OneLink key={index} name={item.name} link={`/room/${index}`}/>
                                })
                            }
                        </div>
                    </BrowserRouter>
                </Provider>
        )
    }
}

ReactDOM.render(
    <App/>,
    document.getElementById('app')
)
